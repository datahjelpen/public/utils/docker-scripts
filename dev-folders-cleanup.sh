#!/bin/bash

echo "🏗  Starting dev folders cleanup script."
echo "You will get to see a list of directories that will be deleted. You will be asked for confirmation before the deletion happens."

echo "What is the name of the folders do you want to delete? Press ENTER for default. (default is: node_modules)"
read dir_to_delete
dir_to_delete=${dir_to_delete:-node_modules}

# Prompt for directory and provide default value if not provided
echo "Where should the script look for these folders? Press ENTER for default. (default is: ~/dev)"
read search_dir
search_dir=${search_dir:-~/dev}
search_dir=$(eval echo $search_dir)

# Ensure the provided argument is a directory
if [ ! -d "$search_dir" ]; then
    echo "❌ Error: $search_dir is not a directory"
    exit 1
fi

# Ask the user for the age of the directories
echo "How many days old should the $dir_to_delete directory be in order to be deleted? Press ENTER for default. (default is: 365)"
read days_old
days_old=${days_old:-365}

# Find and print all $dir_to_delete directories that are older than the specified age
echo "Looking for $days_old+ days old '$dir_to_delete' directories..."
find $search_dir -name $dir_to_delete -type d -prune -mtime +$days_old -exec stat -f "%m %N" {} \; | awk -v now=$(date +%s) '{print int((now-$1)/86400) " days old: " $2}'

# Ask for user confirmation
echo
read -p "⚠️ Do you want to delete these directories? [Y/N]: " -n 1 -r
echo

if [[ $REPLY =~ ^[Yy]$ ]]; then
    # Recursive delete for $dir_to_delete directories
    echo "Deleting directories. Please wait ..."
    find $search_dir -name $dir_to_delete -type d -prune -mtime +$days_old -exec rm -rf {} \;
    echo "✅ Directories deleted successfully."
else
    echo "❌ Operation cancelled. No directories were deleted."
fi
