#!/bin/bash

echo "🏗 Starting Docker cleanup."
echo ""
echo "Current disk usage:"

docker system df
echo ""

while true; do
    read -p "❓ Do you want to remove dangling images? (y/n): " yn
    case $yn in
    [Yy]*)
        docker image prune
        echo "🗑 Dangling images deleted"
        break
        ;;
    [Nn]*)
        echo "⏩ Skipping"
        break
        ;;
    *) echo "ℹ️ Please answer yes (y) or no (n)." ;;
    esac
done

echo ""
echo "✅ Docker dangling images cleanup complete"
echo ""

while true; do
    read -p "❓ Do you want to remove all unused images? (y/n): " yn
    case $yn in
    [Yy]*)
        docker image prune -a
        echo "🗑 All unused images deleted"
        break
        ;;
    [Nn]*)
        echo "⏩ Skipping"
        break
        ;;
    *) echo "ℹ️ Please answer yes (y) or no (n)." ;;
    esac
done

echo ""
echo "✅ Docker all unused images cleanup complete"
echo ""

echo ""
echo "Checking docker volumes ..."
echo ""

docker volume ls
echo ""

docker_volumes=$(docker volume ls -q)

while true; do
    read -p "❓ Do you want to go through the cleanup for volumes (it goes 1 by 1)? (y/n): " yn
    case $yn in
    [Yy]*)
        for volume in $docker_volumes; do
            volume_size=$(docker run --rm -t -v $volume:/volume alpine:latest sh -c "du -sh /volume" | awk '{print $1}')
            echo "💾 Volume: $volume    Size: $volume_size"

            container_id=$(docker ps -a --filter "volume=$volume" --format "{{.ID}}")
            container_name=$(docker ps -a --filter "volume=$volume" --format "{{.Names}}")
            image_repository_tag=$(docker ps -a --filter "volume=$volume" --format "{{.Image}}")

            if [ -n "$container_id" ]; then
                echo "⚠️ Connected to container: $container_id (Name: $container_name)"
                echo "⚠️ Container uses image: $image_repository_tag"
            else
                echo "✅ Volume is not connected to any container"
            fi

            while true; do
                read -p "❓ Do you want to delete this volume? (y/n): " yn
                case $yn in
                [Yy]*)
                    docker volume rm $volume
                    echo "🗑 Deleted volume: $volume"
                    echo ""
                    break
                    ;;
                [Nn]*)
                    echo "⏩ Skipping volume: $volume"
                    echo ""
                    break
                    ;;
                *) echo "ℹ️ Please answer yes (y) or no (n)." ;;
                esac
            done
        done
        break
        ;;
    [Nn]*)
        echo "⏩ Skipping"
        echo ""
        break
        ;;
    *) echo "ℹ️ Please answer yes (y) or no (n)." ;;
    esac
done

echo ""
echo "✅ Docker volume cleanup complete"
echo ""

while true; do
    read -p "❓ Do you want to clean the builder cache? (y/n): " yn
    case $yn in
    [Yy]*)
        docker builder prune -f
        echo "🗑 Docker builder cache pruned"
        break
        ;;
    [Nn]*)
        echo "⏩ Skipping"
        break
        ;;
    *) echo "ℹ️ Please answer yes (y) or no (n)." ;;
    esac
done

echo ""
echo "✅ Docker builder cache cleanup complete"
echo ""
echo ""
echo "✨🐳✨ Docker cleanup complete ✨🐳✨"
echo ""

docker system df
